#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectPushButton_clicked()
{
    ui->disconnectPushButton->setEnabled(true);
    ui->connectPushButton->setEnabled(false);
    receiverThread = new ReceiverThread(0, nullptr);

    connect(receiverThread, &ReceiverThread::receiveImage, [ = ](const QImage & img)
    {
        this->ui->label->setPixmap(QPixmap::fromImage(img));
    });
    connect(receiverThread, &ReceiverThread::serverReady, [ = ]()
    {
        ui->statusbar->showMessage("Ready");
    });
    connect(receiverThread, &ReceiverThread::serverDestroyed, [ = ]()
    {
        ui->statusbar->showMessage("Stopped");
    });

    receiverThread->start();
}

void MainWindow::on_disconnectPushButton_clicked()
{
    ui->disconnectPushButton->setEnabled(false);
    ui->connectPushButton->setEnabled(true);

    receiverThread->stopServer();
}
