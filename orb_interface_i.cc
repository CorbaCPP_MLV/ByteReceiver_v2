#include "orb_interface_i.h"


bytes_transmission_Dessine_i::bytes_transmission_Dessine_i()
{

}
bytes_transmission_Dessine_i::~bytes_transmission_Dessine_i()
{

}

// Methods corresponding to IDL attributes and operations
::CORBA::Boolean bytes_transmission_Dessine_i::sendImage(const bytes_transmission::Image& img)
{
    QByteArray imgData;

    for(CORBA::ULong i = 0; i < img.bytesCount; i++)
    {
        imgData.append(img.data[i]);
    }

    QImage qimg(img.width, img.height, QImage::Format_Indexed8);
    qimg = QImage::fromData(imgData);

    qDebug() << "Received Image (" << img.width << "x" << img.height << ")";

    receiveImage(qimg);
    return true;
}
