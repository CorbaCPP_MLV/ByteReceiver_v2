TEMPLATE = app
CONFIG += console c++11 app_bundl
QT += widgets gui
LIBS += -lboost_system -lboost_signals
LIBS += -lomniORB4 -lomnithread -lomniDynamic4

SOURCES += main.cpp \
    orb_interface_i.cc \
    orb_interfaceSK.cc \
    receiverthread.cpp \
    mainwindow.cpp

HEADERS += \
    orb_interface.hh \
    orb_interface_i.h \
    receiverthread.h \
    mainwindow.h

DISTFILES += \
    orb_interface.idl

FORMS += \
    mainwindow.ui
